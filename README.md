Hah
===

Random "game"

To install, download [rust](https://www.rust-lang.org/tools/install) and then just
```bash
cargo install hah
```

¯\_(ツ)_/¯

TODO: 
- [ ] Add some tests
- [ ] Maybe change the `trail` (background colour) to be a universal mapping of positions => colours.
- [ ] Allow baddies to have trails
- [ ] Improve baddie "AI"
